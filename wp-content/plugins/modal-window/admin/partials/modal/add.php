<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<?php include ('include/data.php'); ?>
<div class="metabox-holder" id="poststuff">
<div class="meta-box-sortables">
<h2><?php esc_attr_e("Modal Window", "umw") ?> <span><?php esc_attr_e("Shortcode", "umw") ?>: <?php echo "[modalsimple id=$id]"; ?></span></h2>
<form action="admin.php?page=wow-modalsimple" method="post" id="addtag">
<div class="postbox">
<div class="inside wow-admin" style="display: block;">	
<div class="wow-admin-col">
<div class="wow-admin-col-12">
<b><?php esc_attr_e("Name", "umw") ?>:</b> <input  placeholder="Name is used only for admin purposes" type='text' name='title' value="<?php echo $title; ?>" />
</div>
</div>
</div>
</div>
<div class="postbox">
    <h3><?php esc_attr_e("Content", "umw") ?></h3>
    <div class="inside wow-admin" style="display: block;">
<div class="wow-admin-col-12">
	<?php wp_editor(stripcslashes($content), 'content', $settings); ?>	
	</div>		
	</div>
	</div>
<div class="postbox">
    <h3><?php esc_attr_e("Close Button", "umw") ?></h3>
    <div class="inside wow-admin" style="display: block;">	
	<div class="wow-admin-col">	
	<div class="wow-admin-col-6"><?php esc_attr_e("Also enable closing on", "umw") ?>:<br>
	<?php esc_attr_e("Overlay", "umw") ?> <input name="close_button_overlay" type="checkbox" value="1" <?php if($close_button_overlay=='1') { echo 'checked="checked"'; } ?>> Esc  <input name="close_button_esc" type="checkbox" value="1" <?php if($close_button_esc=='1') { echo 'checked="checked"'; } ?>>
	</div>
	</div>
</div>	
</div>
	<div class="postbox">
    <h3><?php esc_attr_e("Display", "umw") ?></h3>
    <div class="inside wow-admin" style="display: block;">	
	<div class="wow-admin-col">
	<div class="wow-admin-col-3"><?php esc_attr_e("Show a modal window", "umw") ?>:<br/>
	<select name='modal_show' id="modal_show" onchange="wpchange();">        
        <option value="load" <?php if($modal_show=='load') { echo 'selected="selected"'; } ?>><?php esc_attr_e("When the page loads", "umw") ?></option>
		<option value="click" <?php if($modal_show=='click') { echo 'selected="selected"'; } ?>><?php esc_attr_e("Click on a link (with id)", "umw") ?></option>
		<option value="anchor" <?php if($modal_show=='anchor') { echo 'selected="selected"'; } ?>><?php esc_attr_e("Click on a link (with an #anchor link)", "umw") ?></option>
        <option value="scroll" <?php if($modal_show=='scroll') { echo 'selected="selected"'; } ?>><?php esc_attr_e("When the window is scrolled", "umw") ?></option>
        <option value="close" <?php if($modal_show=='close') { echo 'selected="selected"'; } ?>><?php esc_attr_e("When the user tries to leave the page", "umw") ?></option>		
    </select><br/>
	<div id="wpchange1" style="display:none; width:80%;"><?php echo __("Add an <b>id='wow-modal-id-X'</b> to the link, where X is the number of the modal window", "umw") ?></div>
	<div id="wpchange2" style="display:none; width:80%;"><?php echo __("Add an anchor to the link: <b>a href='#wow-modal-id-X'</b>, where X is the number of the modal window", "umw") ?></div>
	</div>
	<div class="wow-admin-col-3 wpcookie"><?php esc_attr_e("Show only once? (use cookies)", "umw") ?>:<br/>
	<select name='use_cookies'>
        <option value="no" <?php if($use_cookies=='no') { echo 'selected="selected"'; } ?>><?php esc_attr_e("no", "umw") ?></option>
        <option value="yes" <?php if($use_cookies=='yes') { echo 'selected="selected"'; } ?>><?php esc_attr_e("yes", "umw") ?></option>        
    </select>
	</div>
	<div class="wow-admin-col-3 wpcookie"><?php esc_attr_e("Reset in", "umw") ?>:<br/>
	<input type='text'  placeholder="5" name='modal_cookies' value="<?php echo $modal_cookies; ?>"/> <?php esc_attr_e("days", "umw") ?>
	</div>
	<div class="wow-admin-col-3"><?php esc_attr_e("Delay", "umw") ?>:<br/>
	<input type='text'  placeholder="0" name='modal_timer' value="<?php echo $modal_timer; ?>"/> <?php esc_attr_e("seconds", "umw") ?>
	</div>
	</div>
	</div>
	</div>
	<?php submit_button($btn); ?>	
    <input type="hidden" name="addwow" value="<?php echo $hidval; ?>" />    
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	<input type="hidden" name="wowpage" value="<?php echo $wowpage; ?>" />
	<input type="hidden" name="wowtable" value="<?php echo $table_modal; ?>" />	
	<input type="hidden" name="plugdir" value="<?php echo WOW_MODALSIMPLE_PLUGIN_BASENAME; ?>" />	
	<?php wp_nonce_field('wow_action','wow_nonce_field'); ?>	
  </form>
</div>
</div>