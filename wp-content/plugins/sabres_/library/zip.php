<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Zip' ) ) {

    /**
     * The Sabres Zip Class
     *
     * @author Sabres Security inc
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    abstract class SBS_Zip
    {

        public static function extract_file( $full_path, $output_path )
        {
            if ( empty( $full_path ) ) SBS_Error::throwError( 'File path cannot remain empty' );
            if ( empty( $output_path ) ) SBS_Error::throwError( 'Output path cannot remain empty' );

            if ( !file_exists( $full_path ) )
                SBS_Error::throwError( sprintf( 'File not found: %s', $full_path ) );

            if ( !is_dir( $output_path ) )
                SBS_Error::throwError( sprintf( 'Directory not found: %s', $output_path ) );

            if ( !is_writable( $output_path ) )
                @chmod( $output_path, 0755 );

            if ( !is_writable( dirname( $output_path ) ) )
                SBS_Error::throwError( sprintf( 'Can\'t write file: %s Check your permissions!', $output_path ) );

            // Disable time limit
            @ini_set( 'max_execution_time', '0' );
            @set_time_limit( 0 );

            // Disable memory limit
            @ini_set( 'memory_limit', '-1' );

            // Extract zip
            if ( function_exists( 'unzip_file' ) ) {
                $result = unzip_file( $full_path, $output_path );

                if ( is_wp_error( $result ) ) {
                    SBS_Error::throwError( sprintf( 'Bad zip file: %s', $full_path ) );
                }
            } else {
                if ( class_exists( 'ZipArchive' ) ) {
                    // ZipArchive
                    $zip = new ZipArchive;
                    if ( $zip->open( $full_path ) === true ) {
                        $zip->extractTo( $output_path );
                        $zip->close();
                    } else {
                        SBS_Error::throwError( sprintf( 'Bad zip file: %s', $full_path ) );
                    }
                } else {
                    // PclZip
                    @include_once( __DIR__ . '/vendor/pclzip/pclzip/pclzip.lib.php' );

                    if ( class_exists( 'PclZip' ) ) {
                        $zip = new PclZip( $full_path );

                        if ( $zip->extract( PCLZIP_OPT_PATH, $output_path ) == 0 ) {
                            SBS_Error::throwError( sprintf( 'Bad zip file: %s', $full_path ) );
                        }
                    } else {
                        // Zip open
                        if ( function_exists( 'zip_open' ) ) {
                            if ( $zip = zip_open( $full_path ) ) {
                                if ( !is_resource( $zip ) )
                                    SBS_Error::throwError( sprintf( 'Bad zip file: %s', $full_path ) );

                                while ( $zip_entry = zip_read( $zip ) ) {
                                    $zip_entry_name = zip_entry_name( $zip_entry );
                                    $zip_entry_full_path = $output_path . '/' . $zip_entry_name;

                                    if ( substr( $zip_entry_name, -1 ) == '/' || substr( $zip_entry_name, -1 ) == '\\' ) {
                                        if ( !is_dir( $zip_entry_full_path ) )
                                            mkdir( $zip_entry_full_path );
                                    } else {

                                        touch( $zip_entry_full_path );

                                        $zip_entry_file = fopen( $zip_entry_full_path, 'w+' );

                                        if ( zip_entry_open( $zip, $zip_entry ) ) {
                                            $zip_entry_data = zip_entry_read( $zip_entry );

                                            fwrite( $zip_entry_file, $zip_entry_data );
                                            fclose( $zip_entry_file );
                                        }

                                        zip_entry_close( $zip_entry );
                                    }
                                }

                                zip_close( $zip );
                            } else {
                                SBS_Error::throwError( sprintf( 'Bad zip file: %s', $full_path ) );
                            }
                        } else {
                            SBS_Error::throwError( sprintf( 'No zip library could be found' ) );
                        }
                    }
                }
            }

            return true;
        }

        public static function archive( $full_path, $output_file )
        {
            if ( empty( $full_path ) ) SBS_Error::throwError( 'File path cannot remain empty' );
            if ( empty( $output_file ) ) SBS_Error::throwError( 'Output file cannot remain empty' );

            if ( !file_exists( $full_path ) ) {
                SBS_Error::throwError( "Path not found: $full_path" );
            }

            // Disable time limit
            @ini_set( 'max_execution_time', '0' );
            @set_time_limit( 0 );

            // Disable memory limit
            @ini_set( 'memory_limit', '-1' );

            // Check parent
//            if ( !is_writable( $output_file ) )
//                if ( !@chmod( $output_path, 0755 ) ) {
//
//                }
//            if ( !is_writable( dirname( $output_path )) )
//                SBS_Error::throwError( sprintf( 'Can\'t write file: %s Check your permissions!', $output_path ) );


            // Archive
            if ( class_exists( 'ZipArchive' ) ) {
                $zip = new ZipArchive();

                // Delete existing file
                if ( file_exists( $output_file ) ) {
                    if ( !@unlink( $output_file ) ) {
                        SBS_Error::throwError( "Cannot delete file: $output_file" );
                    }
                }

                // Open archive
                $zip->open( $output_file, ZipArchive::CREATE );

                self::archive_folder( $full_path, $zip, $full_path, dirname( $output_file ), true );

                // Close archive
                $zip->close();
            } else {
                @include_once( __DIR__ . '/vendor/pclzip/pclzip/pclzip.lib.php' );

                if ( class_exists( 'PclZip' ) ) {
                    $archive = new PclZip( $output_file );

                    $dir_names = array( ABSPATH );

                    $v_list = $archive->create( $dir_names, PCLZIP_OPT_REMOVE_PATH, ABSPATH,
                        PCLZIP_OPT_ADD_PATH, 'install' );
                    if ( $v_list == 0 ) {
                        die( "Error : " . $archive->errorInfo( true ) );
                    }
                } else {
                    SBS_Error::throwError( sprintf( 'No zip library could be found' ) );
                }
            }
        }


        private static function archive_folder( $source_path, &$zip, $root_path, $target_name, $recursive = false )
        {
            $folders = glob( "$source_path/*", GLOB_ONLYDIR );
            $files = array_filter( glob( "$source_path/*" ), 'is_file' );

            foreach ( $files as $file ) {
                self::archive_item( $file, $zip, $root_path );

//                if (!is_dir($file))
//                {
//                    $map_files[] = array(
//                        "mdf" => md5($file),
//                        "name" => basename($file),
//                        "path" => $source_path,
//                        "size" => $this->formatSizeUnits(filesize( $file )),
//                    );
//                    // var_dump($map_files);
//                    echo '<br />';
//                }
            }

            if ( $recursive ) {
                $path_valid = array( 'wp-admin', 'wp-content', 'wp-includes' );
                $checkFolder = false;

                foreach ( $folders as $folder ) {
                    for ( $i = 0; $i < count( $path_valid ); $i++ ) {
                        $checkFolder = strpos( $folder, $path_valid[$i] );

                        if ( $checkFolder ) {
                            $checkFolder = true;
                            break;
                        }
                    }

                    $pos = strpos( $folder, $target_name );
                    if ( $pos == 0 && $checkFolder ) {
                        self::archive_item( $folder, $zip, $root_path, true );
                        self::archive_folder( $folder, $zip, $root_path, $target_name, $recursive );
                    }
                }
            }
        }

        private static function archive_item( $file, &$zip, $root, $is_dir = false )
        {
            $length = strlen( $root );
            $target_dir = substr( $file, $length + 1 );

            if ( $is_dir ) {
                $zip->addEmptyDir( $target_dir . '/' );
                return;
            }

            $zip->addFile( $file, $target_dir );
        }
    }
}