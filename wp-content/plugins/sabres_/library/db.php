<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_DB' ) ) {

    /**
     * The Sabres Database Class
     *
     * @author Sabres Security inc
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    abstract class SBS_DB {

        public static function get_db_total_size() {
            global $wpdb;

            @$size = $wpdb->get_var( "SELECT SUM(information_schema.tables.data_length) FROM information_schema.tables WHERE table_schema = '" . DB_NAME . "'" );

            if ( is_wp_error( $size ) ) {
                return 0;
            }

            return $size;
        }

        public static function get_current_time() {
            global $wpdb;

            @$timestamp = $wpdb->get_var( "SELECT NOW()" );

            if ( is_wp_error( $timestamp ) ) {
                return 0;
            }

            return strtotime( $timestamp );
        }
    }
}