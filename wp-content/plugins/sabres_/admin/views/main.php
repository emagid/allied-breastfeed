<link href="<?php echo plugins_url( 'styles/bootstrap.css', __DIR__ ); ?>" rel="stylesheet" >
<div class="sabres-admin" >
  <table>
    <tr>
      <td class="sbr-main">

        <div class="sabres-admin-services">
          <strong>Services health</strong>
          <p class="sbr-status">
            <?php
            $websiteSabresServerToken = Sabres::$settings->websiteSabresServerToken;
            $websiteSabresClientToken = Sabres::$settings->websiteSabresClientToken;
			$account_is_attached = false;
            if ( strtolower(Sabres::$settings->addedInPortal) === 'true' ) {
              $account_is_attached = true;
            }
            if (isset($_GET['ui-as-added'])){
              $account_is_attached = true;
            }
            $registration_url = Sabres_Admin::add_website_info(SbrUtils::t('registration_url'));
            $portal_url = Sabres_Admin::add_website_info(SbrUtils::t('portal_url'));

             echo ( Sabres::$settings->server_offline === 'true' )
             ? '<img src="' . plugins_url() . '/sabres/admin/images/error-check.png" alt="" />'
             : '<img src="' . plugins_url() . '/sabres/admin/images/ok-check.png" alt="" />'; ?>
             &nbsp;<?php echo esc_html( 'Connection to gateway' ); ?>&nbsp;
            <?php echo ( empty( $websiteSabresServerToken ) && empty( $websiteSabresClientToken ) )
              ? '<img src="' . plugins_url() . '/sabres/admin/images/error-check.png" alt="" />'
              : '<img src="' . plugins_url() . '/sabres/admin/images/ok-check.png" alt="" />'; ?>
              &nbsp;<?php echo esc_html( 'Service Activated' ); ?>&nbsp;
            <?php echo ( Sabres::$settings->isActive === 'True' )
              ? '<img src="' . plugins_url() . '/sabres/admin/images/ok-check.png" alt="" />'
              : '<img src="' . plugins_url() . '/sabres/admin/images/error-check.png" alt="" />'; ?>
              &nbsp;<?php echo esc_html( 'Traffic monitor' ); ?>
          </p>

          <p class="clearfix sbr-marg-top"><strong>Active Services</strong></p>
            <?php

            $user_plan = "free";
            if ($account_is_attached){
              $user_plan = "registered";
            }

            $boxes = array(
              'free'=> array(
                'website-hardening'=>array('icon'=>'1','text'=>'Website Hardening','l'=>2),
                'firewall-connection'=>array('icon'=>'2','text'=>'Firewall Connection','l'=>2),
              ),
              'registered'=> array(
                'complete-malware-scan'=>array('icon'=>'3','text'=>'Complete Malware Scan','l'=>3),
                'scans--alerts-timing'=>array('icon'=>'4','text'=>'Scans & Alerts Timing','l'=>2),
                'firewall-system-management'=>array('icon'=>'5','text'=>'Firewall System Management','l'=>3),
                'multiple-website-dashboard'=>array('icon'=>'6','text'=>'Multiple Website Dashboard','l'=>3),

              ),
              'paid'=> array(
                'complete-daily-backup'=>array('icon'=>'7','text'=>'Complete Daily Backup','l'=>3),
                'complete-daily-backup1'=>array('icon'=>'8','text'=>'Mobile Management App','l'=>3),
                'automatic-ip-recognition--update'=>array('icon'=>'9','text'=>'Automatic IP Recognition & Update','l'=>3),
                'realtime-firewall-update'=>array('icon'=>'10','text'=>'Realtime Firewall Update','l'=>3),
              )
            );
            $statuses = array('green', 'orange', 'red');

            ?>

            <?php foreach ($boxes as $subject => $sub_boxes): ?>
          <div class="clearfix subject-<?php echo $subject; ?>">
              <?php foreach ($sub_boxes as $box_name => $box): ?>
                <?php
                if ($subject == 'free' || ($account_is_attached)){
                  $box['status'] = $statuses[0];
                } elseif ($subject == 'registered'){
                  $box['status'] = $statuses[1];
                } else {
                  $box['status'] = $statuses[2];
                }
                ?>

              <div class="sbr-box sbr-box-<?php echo $box['status']; ?> ">
                <img src="<?php echo plugins_url(); ?>/sabres/admin/images/icons/<?php echo $box['icon']; ?>.png" />
                <span <?php if ($box['l'] == 2) echo 'style="top:30%;"'; ?>><?php echo $box['text']; ?></span>
              </div>

              <?php endforeach; ?>
          </div>
              <?php if ($user_plan == 'free' && $subject == 'free'): ?>
                <p class="clearfix sbr-marg-top"><strong>Register to activate more services for free!</strong></p>
              <?php elseif ($user_plan == 'free' && $subject == 'registered'): ?>
                <div class="clearfix sbr-marg-top">
                  <a target="_blank" href="<?php echo $registration_url; ?>" class="sabres-admin-btn">Register</a>
                  <p>
                    <!-- Choose one of our premium plans for more features -->
                  </p>
                </div>
              <?php elseif ($user_plan != 'free' && $subject == 'registered'): ?>
                <div class="clearfix">
                  <!--<a target="_blank" href="<?php echo $portal_url ?>" class="sabres-admin-btn">Open dashboards</a>-->
                </div>
              <?php elseif ($user_plan != 'paid' && $subject == 'paid'): ?>
                <div class="clearfix">
                  <!-- <a href="#buy-now" class="sabres-admin-btn">Buy Now ??</a> -->
                </div>
              <?php endif; ?>

            <?php endforeach; ?>

            <div class="form-sabres_activate">
<?php if (!$account_is_attached): ?>
            <strong>
              Use this key to add your website to <?php echo SbrUtils::t('name'); ?> dashboard:
            </strong>
<?php endif; ?>
            <table class="">
              <tbody>
                <tr>
                  <td>
                    <p>
                      Your Api-Key:
                      <br>
                      <code>
                        <?php echo Sabres::$settings->apiKey; ?>
                      </code>
                    </p>
<?php if (!$account_is_attached): ?>
                    <p>
                      <a target="_blank" href="<?php echo $registration_url; ?>" class="sabres-admin-btn">Register</a>
                    </p>
<?php endif; ?>
                  </td>
                  <td class="sbr-explanation">
<?php if (!$account_is_attached): ?>
                    <p>
                      <img class="sbr-img-explanation" src="<?php echo plugins_url(); ?>/sabres/admin/images/explanation.png" alt="" />
                    </p>
<?php endif; ?>
                  </td>
                </tr>
              </tbody>
            </table>

            </div>

            <div class="form-sabres_reset">
              <strong><?php echo esc_html( 'Reset '.SbrUtils::t('name').' Activation' ); ?></strong>
              <p><?php echo esc_html( 'Reset the activation data and reactivate '.SbrUtils::t('name').'.' ); ?>
                <?php
                    if ( !empty( $_POST['sbsreset'] ) ) {
                        if ( Sabres_Admin::reset_sabres_activation() ) {
                            echo '<img src="'.plugins_url().'/sabres/admin/images/ok-check.png" alt="" />&nbsp;&nbsp;';
                            echo esc_html( 'Activation has been reset successfully.' );
                        } else {
                            echo '<img src="'.plugins_url().'/sabres/admin/images/error-check.png" alt="" />&nbsp;&nbsp;';
                            echo esc_html( 'Activation reset has failed.' );
                        }
                    }
                ?>
                <form name="sabres_reset" action="" method="post">
                <input type="submit" name="sbsreset" style="margin-left:10px;" class="right sabres-admin-btn sabres-admin-btn-rect" value="Reset Activation" onclick="<?php echo htmlspecialchars( "return confirm('Are you sure?');", ENT_QUOTES, 'UTF-8' ); ?>">
                </form>
              </p>
            </div>

            <div class="clear"></div>
        </div>
      </td>
    <td class="sbr-aside">
      <div class="offer">
  <div class="offer-inner">
    <p>
      <?php if ($account_is_attached): ?>
        Website added successfully
      <?php else: ?>
        Create your <?php echo SbrUtils::t('name'); ?> Account and get access to additional free security components and dashboard management tool.
      <?php endif; ?>
    </p>
    <?php if ($account_is_attached): ?>
      <a target="_blank" href="<?php echo $portal_url; ?>" class="offer-btn">Open Dashboard</a>
    <?php else: ?>
      <a target="_blank" href="<?php echo $registration_url; ?>" class="offer-btn">Register Now</a>
    <?php endif; ?>
  </div>
  <div class="offer-badge-outer">
    <div class="offer-badge-inner">
      <?php if ($account_is_attached): ?>
        <small>Active</small>
      <?php else: ?>
        Free
      <?php endif; ?>
    </div>
  </div>
</div>

    </td>
  </tr>
</table>
</div>


