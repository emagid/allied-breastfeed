<?php

if ( !empty( $_POST['a'] ) ) {
    $action = trim($_POST['a']);

    switch ($action) {
        case 'sc':
            require_once __DIR__ . '/library/include.php';

            SBS_Cache::disable_cache();

            $base_path = preg_replace( '/wp-content(?!.*wp-content).*/','', __DIR__ );
            require_once $base_path . 'wp-load.php';

            break;
    }
}