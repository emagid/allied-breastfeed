<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

require_once SBR__PLUGIN_DIR . '/_inc/modules/class.scanner.engine.php';
require_once SBR__PLUGIN_DIR . '/_inc/modules/class.scanner.php';

require_once SBR__PLUGIN_DIR . '/_inc/modules/class.malware.scanner.engine.php';
require_once SBR__PLUGIN_DIR . '/_inc/modules/class.security.scanner.engine.php';