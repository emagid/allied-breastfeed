<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

require_once SBR__PLUGIN_DIR . '/_inc/modules/class.backup.php';