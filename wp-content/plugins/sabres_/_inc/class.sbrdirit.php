<?php

abstract class FilesystemRegexFilter extends RecursiveRegexIterator {
    protected $regex;
    public function __construct(RecursiveIterator $it, $regex) {
        $this->regex = $regex;
        parent::__construct($it, $regex);
    }
}


class FilenameFilter extends FilesystemRegexFilter {
    // Filter files against the regex
    public function accept() {
        return ( ! $this->isFile() || preg_match($this->regex, $this->getFilename()));
    }
}

class DirnameFilter extends FilesystemRegexFilter {
    // Filter directories against the regex
    public function accept() {
        return ( ! $this->isDir() || preg_match($this->regex, $this->getFilename()));
    }
}

/*
class SbrFileStruct extends SplFileInfo {
	public function __toString (){
		return (string) self::getOwner();
	}
}
*/

class SbrDirIt {

    public function __construct()
    {
        $this->base_path = str_replace('\\', '/', ABSPATH);
    }

    private static $allowed_extensions = array(
        'php',
        'js',
        'htm',
        'html'
    );

    public function recur_dir_it($root, $max_depth){
        $dir = new RecursiveDirectoryIterator($root, FilesystemIterator::SKIP_DOTS);

        // Flatten the recursive iterator, folders come before their files
        $it  = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::SELF_FIRST);

        // Maximum depth is 1 level deeper than the base folder
        $it->setMaxDepth($max_depth);

        $arr = array();
        foreach ($it as $fileinfo) {
            $arr[] = $this->get_file_info($fileinfo);
        }
        return $arr;
    }


    public function recur_dir_it_filtered($root, $dir_filter, $file_filter, $max_depth){
        $directory = new RecursiveDirectoryIterator($root, FilesystemIterator::SKIP_DOTS);
        // Filter out "ignor*" folders
        $filter = new DirnameFilter($directory, $dir_filter);// '/^(?!ignor)/');
        // Filter PHP/HTML/txt files
        $filter = new FilenameFilter($filter, $file_filter);// '/\.(?:php|html|txt)$/');

        $arr = array();

        // print($fileinfo->getFileInfo('SbrFileStruct') . "\n");

        // foreach(new RecursiveIteratorIterator($filter) as $fileinfo) {

        $files = new RecursiveIteratorIterator($filter);
        $files->setMaxDepth($max_depth); // Two levels, the parameter is zero-based.
        foreach($files as $fileinfo) {

            $arr[] = $this->get_file_info($fileinfo);
        }
        return $arr;
    }

    private function get_file_info($fileinfo)
    {
        return array(
            'fn'=>$this->get_relative_path((string) $fileinfo),
            'ac'=>$this->get_date_time($fileinfo->getATime()),
            'md'=>$this->get_date_time($fileinfo->getMTime()),
            'cg'=>$this->get_date_time($fileinfo->getCTime()),
            'gid'=>$fileinfo->getGroup(),
            'uid'=>$fileinfo->getOwner(),
            'pr'=>decoct($fileinfo->getPerms()),
            'sz'=>$fileinfo->getSize(),
            'ex'=>(int) $fileinfo->isExecutable()
        );
    }

    private function get_date_time($unix_time)
    {
        return date("Y-m-d H:i:s", $unix_time);
    }

    private function get_relative_path($path)
    {
        return str_replace(array('\\',$this->base_path), array('/', ''), $path);
    }

    public function get_core_files_list( ) {
        $it = new RecursiveIteratorIterator(
            new RecursiveCallbackFilterIterator(
                new RecursiveDirectoryIterator( ABSPATH, FilesystemIterator::SKIP_DOTS ), function ($fileInfo, $key, $iterator) {
                    $relative_path = trim( str_replace( realpath( ABSPATH ), '', dirname( $fileInfo->getRealPath() ) ), '/\\' );

                    if ( !is_dir( $key ) ) {
                        $file_name = $fileInfo->getFilename();
                        $file_ext = pathinfo( $file_name, PATHINFO_EXTENSION );

                        if ( !in_array( $file_ext, self::$allowed_extensions) ) {
                            return false;
                        }
                    }

                    return ( $relative_path == '' || stripos( $relative_path, 'wp-' ) === 0 );
                }
            )
        );

        $arr = array();
        foreach ($it as $fileinfo) {
            $arr[] = $this->get_file_info($fileinfo);
        }

        return $arr;
    }

    private $base_path;
}