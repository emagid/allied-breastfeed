<?php
///
/// Created: 12/01/2016
/// By: Ronny Sherer
/// Purpose: General purpose utilities of the plugin.
///
class SbrUtils
{
	static public function associated_array_to_obj($arr)
	{
		$obj = (object)$arr;
		foreach ($arr as &$value)
		{
			if (is_array($value))
				$value = (object)$value;
		}
		return $obj;
	}

	static public function obj_to_associated_array($obj)
	{
		$arr = (array)$obj;
		foreach ($arr as &$value)
		{
			if (is_object($value))
				$value = (array)$value;
		}
		return $arr;
	}

	static public function get_json($arr)
	{
		return json_encode( self::associated_array_to_obj($arr) );
	}

	static public function set_debug($is_debug)
	{
		self::$debug = $is_debug;
	}

	static public function debug_log($str)
	{
		file_put_contents(dirname(dirname(__FILE__)).'/debug.log', date("Y-m-d H:i:s")." - $str\n", FILE_APPEND);
	}

	static private $debug = false;

	private static $conf_instance;
	public static function t($name, $group=''){
		if (!self::$conf_instance){
			self::load_conf();
		}
		if ($group && isset(self::$conf_instance[$group]) && isset(self::$conf_instance[$group][$name])){
			return self::$conf_instance[$group][$name];
		} elseif (isset(self::$conf_instance[$name])) {
			return self::$conf_instance[$name];
		}
		return NULL;
	}
	protected static function load_conf(){
		$filename = dirname(dirname(__FILE__)).'/conf/conf.yaml';

		self::$conf_instance = spyc_load_file($filename);

	}
}
