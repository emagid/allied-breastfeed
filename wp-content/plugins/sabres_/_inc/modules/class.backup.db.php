<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Backup_DB_Engine' ) ) {
    /**
     * The Sabres Backup Database Engine Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Backup_DB_Engine
    {
        private $settings;
        private $dump_file_name;

        public function __construct() { }

        public function register_event_callback( $event_name, $callback ) {
            if ( !isset( $this->_event_callbacks[$event_name] ) ) {
                $this->_event_callbacks[$event_name] = array();
            }

            array_push( $this->_event_callbacks[$event_name], $callback );
        }

        private function event_trigger( $event_name, $args ) {
            if ( !empty( $this->_event_callbacks[$event_name] ) ) {
                $callbacks = $this->_event_callbacks[$event_name];

                foreach ( $callbacks as $callback ) {
                    if ( !empty( $args ) ) {
                        call_user_func_array( $callback, $args );
                    } else {
                        call_user_func( $callback );
                    }
                }
            }
        }

        public function init( $settings = null, $dump_file_name ) {
            if ( !empty( $settings ) ) {
                $this->settings = (object) array_change_key_case( $settings, CASE_LOWER );
            }
            $this->dump_file_name = $dump_file_name;
        }

        public function is_valid() {
            $is_valid = true;

            // Check PDO
            if ( !class_exists('PDO') ) {
                $is_valid = false;
            }

            // Check vendor
            if ( !class_exists( 'Ifsnop\Mysqldump\Mysqldump' ) ) {
                $is_valid = false;
            }

            // Check variables
            if ( empty( $this->dump_file_name ) ) {
                $is_valid = false;
            }

            // Check is free disk
            $db_size = SBS_DB::get_db_total_size();
            $free_space = disk_free_space( ABSPATH );

            if ( $db_size >= $free_space ) {
                $is_valid = false;
            }

            return $is_valid;
        }

        public function run() {
            if ($this->is_valid()) {
                // Disable time limit
                @ini_set( 'max_execution_time', '0' );
                @set_time_limit( 0 );

                // Disable memory limit
                @ini_set( 'memory_limit', '-1' );

                $dumpSettings = array(
                    'compress'=> \Ifsnop\Mysqldump\Mysqldump::GZIP
                );

                try {
                    $dump = new Ifsnop\Mysqldump\Mysqldump( 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASSWORD, $dumpSettings );
                    $dump_dir = ABSPATH . '/wp-content/sbs-backup';

                    if ( !is_dir( $dump_dir ) ) {
                        if ( !mkdir( $dump_dir ) ) {
                            $this->event_trigger( 'error', array(
                                'desc' => "No permissions to create folder: $dump_dir"
                            ) );

                            return;
                        }
                    }

                    $dump_file_name = $dump_dir . '/' . $this->dump_file_name .'.sql.gz';
                    $dump->start( $dump_file_name );


                    // Move to wp-content
//                    $root_dir = realpath( ABSPATH );
//                    $move_from = $dump_file_name;
//                    $move_to = $root_dir . '\wp-content\sbs-backup\\' . $this->dump_file_name . '.sql.zip';
//
//
//                    // Delete existing file
//                    if ( file_exists( $move_to ) ) {
//                        if ( !@unlink( $move_to ) ) {
//                            throw new Exception( "Cannot delete file: $move_to" );
//                        }
//                    }
//
//                    rename( $move_from, $move_to );

//                  echo 'Backup DB done succesfully!';

                } catch (\Exception $e) {
                    $this->event_trigger( 'error', array(
                        'desc' => $e->getMessage()
                    ) );
//                    echo 'Backup DB is failed';
                }
            }
        }
    }
}