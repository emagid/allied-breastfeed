<?php get_header(); ?>
 	<div id="heroImage" style="background-image:url('/wp-content/uploads/2016/05/breastfeeding-52026.jpg')"></div>

 	<div id="main">
 		<div id="welcomeSection">
 			<h1>My Practice</h1>
 			<p><?php the_field('welcome_text'); ?></p>
 		</div>
 		<div id="doctor"></div>

 		<div class="doctor-wrapper">
 			<?php
	  			$args = array(
	    		'post_type' => 'meetourdoctors',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

		<div class="meetSection" id="meetTheDoctor">
	 		<div id="meetImage">
	 			<img src="<?php the_field('image'); ?>" />
	 		</div>
	 		<div id="meetText">
	 			<h1><?php the_field('doctor'); ?></h1>
				<p><?php the_field('text'); ?></p>
	 		</div>
		</div>

		<?php
			}
				}
			else {
			echo 'No Doctors Found';
			}
		?>
		</div>
		<div id="policy"></div>
		<div id="policySection">
			<h1>Office Policies</h1>
				 	<?$id = 253;
				 	$post = get_post($id); ?>
				<p><?php the_field('office_policies'); ?></p>


 		</div>
		<div id="policySection">
			<h1>MOM STORIES/TESTIMONIALS</h1>
				 	<?$id = 253;
				 	$post = get_post($id); ?>
				<p><?php the_field('moms_stories'); ?></p>


 		</div>
 		<div id="policySection">
			<h1>Patient Resources</h1>
			<p><i>The following documents are downloadable.</i></p>
			<p>1. <a href="/wp-content/uploads/2017/06/GENERAL-Intake-1-2017-06-08.pdf">General Intake Form</a></p>
						<p>2. <a href="/wp-content/uploads/2017/06/TWIN-Intake-1-2017-06-08.pdf">Twin Intake Form</a></p>
			<p>3. <a href="/wp-content/uploads/2017/06/Dr.-Lauren-HIPAA-2017-1-2017-06-08.pdf">HIPAA 2017</a></p>



 		</div>
 		<div id="hours"></div>
 		 	<?php
	  			$args = array(
	    		'post_type' => 'office-hours',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
 		<div id="hoursSection">
 			<h2><?php the_field('office_name'); ?></h2>
 			<p><?php the_field('comments'); ?></p>
 			<div id="hoursIcon">
 				<img src="<?php bloginfo('template_directory'); ?>/images/clock-icon.png">
 				<h1>Hours</h1>	
 			</div>

 			<div id="hoursSchedule">



 				<table>
 
				  <tr>
				    <td>Monday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('monday'); ?></td>
				  </tr>

				  <tr>
				    <td>Tuesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('tuesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Wednesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('wednesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Thursday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('thursday'); ?></td>
				  </tr>

				  <tr>
				    <td>Friday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('friday'); ?></td>
				  </tr>

				</table>

 			</div> <!-- hoursSchedule -->
 			<p><?php the_field('sub_comments'); ?></p>
 			</div> <!-- hoursSection -->
			<?php
				}
					}
				else {
				echo 'No Text Found';
				}
			?>


 			<div id="plansSection">
 				<h1>Insurance Plans</h1>
 				<div id="plansProviders">
 			<?php
	  			$args = array(
	    		'post_type' => 'insurance-plans',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
				<?php the_field('insurance_content'); ?>
 			<?php
				}
					}
				else {
				echo 'No Plans Found';
				}
			?>	
 				</div>	
 			</div>

 			<div id="contact"></div>
 			<div id="contactSection">

 				<h1>Connect With Us</h1>
 				<h6 class="email-link">
 					</h6>
 				<div class="contactDivs">
	 				<div class="contactBlock">
	 					<?$id = 42;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/pinpoint-icon.png">
	 					
	 					<?=$content?>

	 				</div>
	 				<div class="contactBlock contactPhone">
	 					<?$id = 45;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/phone-icon.png">
	 					<h6><?=$content?></h6>

	 				</div>
	 				<div class="contactBlock">
<iframe height="90%" width="90%" border="0" marginwidth="0" marginheight="0" src="https://www.mapquest.com/embed/us/ny/new-hyde-park/11040-2501/1575-hillside-ave-40.747698,-73.686133?center=40.74826999999999,-73.68647&zoom=15&maptype=undefined"></iframe>
	 				</div>
	 				<div class="contactBlock">
	 					<div id="fb">		<?php
							if(is_active_sidebar('sidebar-1')){
							dynamic_sidebar('sidebar-1');
							}
							?>
						</div>
	 				</div>
				</div>
</div>

 				<div id="sponsorBox">
 					<img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png">
 					<h6>Allied Physicians Group</h6>
 					<button><a href="http://alliedphysiciansgroup.com/">Visit</a></button>
 				</div>

 			</div>

<?php get_footer(); ?>
